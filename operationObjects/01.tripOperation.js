const po = require('../pageObjects/01.tripPageObjects.js');
const fs = require('fs');
let td=getTestData();
//include the selenium-webdriver
const webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until;
    WebElement= webdriver.WebElement;

// Promise.all([
//         runTest()
//       ]).then(_ => {
//         console.log('As promised, all done!');
//       }, err => {
//         console.error('An error occured! ' + err);
//         setTimeout(() => {throw err}, 0);
// });
    
async function runTest() {
    //new instance of a driver
    let driver = new webdriver.Builder()
        .forBrowser(td.browser)
        .build();    
    
    //document to test
try{
            await driver.get(td.pathURL);
            
            let txtPlan=await driver.findElement(By.css(po.landingTabName)).getText();
            await driver.findElement(By.css(po.fromStop)).sendKeys(td.testFromStop);
            await driver.findElement(By.css(po.toStop)).sendKeys(td.testToStop);
            let goBtnTxt=await driver.findElement(By.css(po.goButton)).getText();
            var s1= getScreenshot("../screenshots/screenshot11.png",driver);
            await driver.findElement(By.css(po.goButton)).click();
            var s2= getScreenshot("../screenshots/screenshot21.png",driver);
    
            let resultSet = await (await driver.wait(until.elementLocated(By.xpath(po.resultList), 5000))).getText();
             // await driver.findElement(By.css('#trip-transport-filter-tab__public-transport')).click();
              
        
        //   let lblPublic = driver.findElement(By.xpath("//*(text()='Public transport'"))
    
            // driver.findElements(By.xpath("//*[contains(@class,'mode-list ng-star-inserted')]")).then(function(elems){
            // var v_length=elems.length;
            // console.log(v_length);
            // } );
            return; //-------------added later--------------------*********
} catch (ex) {
            console.log('An error occured! ' + ex);
          } finally {
            await driver.quit();
          }
};
    

function getTestData()
{
    const dataBuffer=fs.readFileSync('../GUI-Test-Jest-Selenium/testData/testDataSearchStop.json');
    const dataJSON=dataBuffer.toString();
    const testData= JSON.parse(dataJSON);
    return(testData);
}

function getScreenshot(fileName, driver)
{
if(td.takeScreenShot== true){
    driver.takeScreenshot().then(
        function(image, err) {
            //require('fs').writeFile(fileName, image, 'base64', function(err) {
             require('fs').writeFile(fileName, image, 'base64', function(err) {
            console.log(err);
            });
        }
    );
}
return;
}


module.exports = 
{
       runTest : runTest
};