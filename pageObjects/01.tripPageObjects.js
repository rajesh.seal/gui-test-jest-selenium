module.exports = 
{
        landingTabName: "#tab-pane-tp",
        fromStop: "#tniFromTripLocation",
        toStop: "#tniToTripLocation",
        goButton: "button.btn:nth-child(2)",
        resultList: "//div[@class='results']"
};