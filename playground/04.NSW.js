//include the selenium-webdriver
const webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until;
    WebElement= webdriver.WebElement;

  //  const angularReadyScript = "return angular.element(document).injector().get('$http').pendingRequests.length === 0";

async function runTest() {
//new instance of a driver
let driver = new webdriver.Builder()
    .forBrowser('firefox')
    .build();    

//document to test
    try{
        await driver.get('https://transportnsw.info/');
        await driver.findElement(By.css('#tniFromTripLocation')).sendKeys('Rockdale Station, Rockdale');
        await driver.findElement(By.css('#tniToTripLocation')).sendKeys('Hurstville Station, Hurstville');
            // await driver.takeScreenshot().then(
            //           function(image, err) {
            //               require('fs').writeFile('fill.png', image, 'base64', function(err) {
            //                   console.log(err);
            //               });
            //           }
            //   );
              

// click on go button to search 
        await driver.findElement(By.css('button.btn:nth-child(2)')).click();

        // await driver.takeScreenshot().then(
        //   function(image, err) {
        //       require('fs').writeFile('search.png', image, 'base64', function(err) {
        //           console.log(err);
        //       });
        //   }
        // );

        await (await driver.wait(until.elementLocated(By.xpath("//div[@class='results']"), 5000))).click

        // await driver.findElement(By.css('#trip-transport-filter-tab__public-transport')).click();
      
      //rase  await driver.findElement(By.xpath("//div[@class='results']")).click();

    
    //   let lblPublic = driver.findElement(By.xpath("//*(text()='Public transport'"))

        // driver.findElements(By.xpath("//*[contains(@class,'mode-list ng-star-inserted')]")).then(function(elems){
        // var v_length=elems.length;
        // console.log(v_length);
        // } );

    } catch (ex) {
        console.log('An error occured! ' + ex);
      } finally {
        await driver.quit();
      }
};

    Promise.all([
        runTest()
      ]).then(_ => {
        console.log('All done!');
      }, err => {
        console.error('An error occured! ' + err);
        setTimeout(() => {throw err}, 0);
      });